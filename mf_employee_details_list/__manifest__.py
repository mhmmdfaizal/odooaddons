# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Employee List',
    'version': '1.1',
    'author': 'Muhammad Faizal NS',
    'category': 'Hr',
    'website': 'https://www.facebook.com/mhmmdfaizal',

    'depends': ['base','hr','report'],
    'data': [
            'report/mf_menu.xml',
            'report/mf_employee_list.xml',
            'wizard/mf_employee_list.xml',
            ],

    'demo': [],
    'installable': True,
    'auto_install': False,
}