from odoo import api, fields, models
from odoo.exceptions import UserError

class EmployeeList(models.TransientModel):
	_name = "employee.list"
	_description = "Employee List"

	company_id = fields.Many2one('res.company', string='Company', required=True)

	@api.multi
	def generated_employee_list(self):
		employee_search = self.env['hr.employee'].search([('company_id', '=', self.company_id.id)])
		return self.env["report"].get_action(employee_search, 'mf_employee_details_list.report_employee_new')
